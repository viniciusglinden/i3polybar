
" automatically install vim plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" --------------------------------------------------------------------------
"	PLUGINS
" vim-plug plugin manager
" download it first, by running the provided curl script 
call plug#begin()
	Plug 'chriskempson/base16-vim'
	Plug 'preservim/nerdcommenter' " automatic line commenter
	Plug 'ycm-core/YouCompleteMe' " programming language syntax (YCM)
	Plug 'sirver/ultisnips' " provides language syntax
	"Plug 'airblade/vim-gitgutter' " marks line status in git
	"Plug 'tpope/vim-repeat' " extend the '.' key
	Plug 'preservim/nerdtree' " project tree
	Plug 'scrooloose/syntastic' " syntax lightlights and error displays
	Plug 'vim-airline/vim-airline' " fancy status bar and top buffer display
	Plug 'vim-airline/vim-airline-themes'
	Plug 'suoto/vim-hdl' " VHDL support
	" leaving you to install FZF by youself!
	"Plug 'junegunn/fzf' " fuzzy finder (terminal command)
	"Plug 'junegunn/fzf.vim' " fzf configuration for VIM
	Plug '/usr/local/fzf' " points to the FZF installation
	Plug 'rbgrouleff/bclose.vim' " easy buffer delete
	"Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' } " live LaTeX preview (only in UNIX)
	"Plug 'godlygeek/csapprox' " translate from gvim theme
call plug#end()

" --------------------------------------------------------------------------
"	REGULAR SETTINGS
set nu rnu ruler " set up line counter
set tabstop=4 " tabs appear as 4 spaces
set shiftwidth=4
set softtabstop=0 " Setting this to a non-zero value other than tabstop will make the tab key (in insert mode) insert a combination of spaces (and possibly tabs) to simulate tab stops at this width.
set noexpandtab " Enabling this will make the tab key (in insert mode) insert spaces instead of tab characters
set showmatch " displays matching ([{...
set mat=1 " 0.1s blink when matched
set showcmd " displays incomplete commands
set wildmenu " displays complete matches in a status line
set cursorline " highlights current line

" theme configuration
set t_Co=256
let base16colorspace=256 " access colors present in 256 colorspace
"colorscheme monokai-phoenix " commented out line 'hi Comment...'
set background=dark
" allows transparent background
"hi Normal guibg=NONE ctermbg=NONE

filetype indent on
filetype plugin on
"syntax on " enable syntax window

set title " displays title in terminal header
set encoding=utf-8 " UTF-8 encoding
set ffs=unix,dos,mac " use UNIX standard file type
set noswapfile " disable the creation of swap files (lock)
set ai " auto indent
set si " smart indent
set wrap " wrap lines

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
	\ if line("'\"") > 0 && line("'\"") <= line("$") |
	\   exe "normal! g`\"" |
	\ endif

set viminfo^=% " Remember info about open buffers on close

set incsearch hlsearch
set nocompatible " VIM instead of Vi
set listchars=eol:¶,trail:.,tab:→→,extends:>,precedes:<
set list " activate listchars
set autoindent " automatic indentation
set backspace=2 " backspace in insert mode is normal, shorthand for: =indent,eol,start
" disables syntax check for markdown
"au BufRead *.md set ft=

" define leader key
"let mapleader = "\"

" map 10 up-down
noremap <C-k> : -10<CR> zz
noremap <C-j> : +10<CR> zz
" map ctrl-o to open line in normal mode
nnoremap <c-o> o<esc>$d^

" Live LaTeX preview
"nnoremap <F1> :LLPStartPreview<CR>

" buffer operations
noremap <F3> :bn!<CR>
noremap <F2> :bp!<CR>
noremap <F4> :Bclose<CR>

" split windows switching
"noremap <F12> <C-w>w

" remap Y to y$, like C is c$
noremap Y y$

" disable some default maps
map + <Nop>
map - <Nop>
map <s-k> <Nop>
map <c-b> <Nop>

" custom shortcuts
inoremap ¬ <esc>/<++><enter>"_c4l
noremap ¬ /<++><enter>"_c4l
autocmd FileType vhdl source ~/.dotfiles/.vim/vhdl_mappings
autocmd FileType markdown source ~/.dotfiles/.vim/markdown_mappings

"		Fuzzy finder
"nmap <leader>f : Files<CR>
" leaving you to install FZF by youself!
nmap <leader>f : FZF<CR>

"		Gitgutter
"autocmd vimenter * GitGutterEnable " Not to be confused with GitGutterBufferEnable

"	vim-airline
let g:airline_theme='dark'
" if not correctly displayed, please install powerline fonts
let g:airline_powerline_fonts = 1
"let g:airline_left_sep = '⮀'
"let g:airline_left_alt_sep = '⮁'
"let g:airline_right_sep = '⮂'
"let g:airline_right_alt_sep = '⮃' " enables tabline

let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#tabline#left_sep = '⮀'
"let g:airline#extensions#tabline#left_alt_sep = '⮀'

"		NERDtree
let NERDTreeIgnore = ['\.pyc$', '\.o$', '\.out$']
nmap <leader>tt :NERDTreeToggle<CR>
"		You Complete Me
" unecessary for regular syntax search
"let g:ycm_global_ycm_extra_conf = "~/.dotfiles/.vim/ycm_extra_conf.py"
