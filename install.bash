#!/bin/bash
# Author: Vinícius Gabriel LINDEN
# Date: 27/feb/2020
# Description: Autoinstall dotfiles
# TODO:
#  Fix .config for root
#  Fix --super

echo "This is $(basename "$0") script"

# exit script at first error
set -e

echo "This script has issues, check if it works!"

help () {
	echo ""
	echo "If no option is given, the following configuration files will not be installed:"
	echo " > Zathura, Ranger, URXVT"
	echo ""
	echo "OPTIONS:"
	echo "    --super: also install it for super-user"
	echo "    --all: install every optional script"
	echo "    --zathura"
	echo "    --ranger"
	echo "    --tmux"
	echo "    --urxvt"
	echo "    --i3polybar: install i3 with polybar"
	echo ""
	echo "FULL INSTALLATION:"
	echo "    ./install.sh --zathura --ranger"
	echo ""
}

# interpret user entries
for i in "$@"
do
	case $i in
		--help)
			help
			exit 0
			;;
		--super)
			SUPER=true
			;;
		--all)
			ZATHURA=true
			RANGER=true
			TMUX=true
			URXVT=true
			I3POLYBAR=true
			;;
		--zathura)
			ZATHURA=true
			;;
		--ranger)
			RANGER=true
			;;
		--tmux)
			TMUX=true
			;;
		--urxvt)
			URXVT=true
			;;
		--i3polybar)
			I3POLYBAR=true
			;;
		*)
			echo "The option you typed is invalid!"
			exit 126
			;;
	esac
done

# get .sh directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
USER="$HOME"
ROOT=/root

# tests if user has placed dotfiles folder correctly
if [ ! "$DIR" == "$USER/.dotfiles" ]; then
	echo "This folder has to be placed at ~/.dotfiles";
	echo "Not installing!";
	exit 2
fi

[ "$SUPER" == "true" ] && echo "Also installing for super user"

if [ "$ZATHURA" == "true" ]; then
	rm -rf "$HOME"/.config/zathura
	echo "Zathura..."
	ln -sf "$DIR"/.config/zathura "$HOME"/.config
	[ "$SUPER" == "true" ] && ln -sf "$DIR"/.config/zathura "$ROOT"/.config
fi

if [ "$RANGER" == "true" ]; then
	rm -rf "$HOME"/.config/ranger
	echo "Ranger..."
	ln -sf "$DIR"/.config/ranger "$HOME"/.config/
	[ "$SUPER" == "true" ] && ln -sf "$DIR"/.config/ranger "$ROOT"/.config
fi

if [ "$TMUX" == "true" ]; then
	echo ".tmux..."
	ln -sf "$DIR"/.tmux.conf "$HOME"
	[ "$SUPER" == "true" ] && ln -sf "$DIR"/.tmux.conf "$ROOT"
fi

if [ "$URXVT" == "true" ]; then
	echo "URXVT..."
	ln -sf "$DIR"/.urxvt "$HOME"
	[ "$SUPER" == "true" ] && ln -sf "$DIR"/.urxvt "$ROOT"
	echo ".Xdefaults..."
	ln -sf "$DIR"/.Xdefaults "$HOME"
	[ "$SUPER" == "true" ] && ln -sf "$DIR"/.Xdefaults "$ROOT"
fi

if [ "$I3POLYBAR" == "true" ]; then
	echo "i3..."
	ln -sf "$DIR"/.config/i3 "$HOME"/.config
	echo "polybar..."
	ln -sf "$DIR"/.config/polybar "$HOME"/.config
	if [ "$SUPER" == "true" ]; then
		ln -sf "$DIR"/.config/i3 "$ROOT"/.config
		ln -sf "$DIR"/.config/polybar "$ROOT"/.config
	fi
fi

echo ".bashrc..."
ln -sf "$DIR"/.bashrc "$HOME"
[ "$SUPER" == "true" ] && ln -sf "$DIR"/.bashrc "$ROOT"
echo ".zshrc..."
ln -sf "$DIR"/.zshrc "$HOME"
[ "$SUPER" == "true" ] && ln -sf "$DIR"/.zshrc "$ROOT"
echo ".vimrc..."
ln -sf "$DIR"/.vimrc "$HOME"
[ "$SUPER" == "true" ] && ln -sf "$DIR"/.vimrc "$ROOT"

echo "Installation completed"

