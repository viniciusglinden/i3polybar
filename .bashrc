# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# PS1 (user input)
export PS1="\[$(tput bold)\]\[\033[38;5;4m\]\h\[$(tput sgr0)\]\[\033[38;5;3m\]@\[$(tput sgr0)\]\[\033[38;5;10m\]\u\[$(tput sgr0)\]\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;6m\]\W\[$(tput bold)\]\[$(tput sgr0)\]\[\033[38;5;3m\]\\$\[$(tput sgr0)\]"

# alias
source ~/.dotfiles/.common_bash_zsh
[ -f ~/.common-bash-zsh-specific ] && source ~/.common-bash-zsh-specific

# fzf plugin
#[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# pc specific
[ -f ~/.bashrc-specific ] && source ~/.bashrc-specific

set -o vi # sets vim-like input

