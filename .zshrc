# Enable colors and change prompt: (PS1 = PROMPT in zsh)
autoload -U colors && colors
PROMPT="%F{21}%M%B%F{3}@%b%{$fg[green]%}%n %{$fg[cyan]%}%~%F{3}%B%#%b %{$reset_color%}"

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots) # Include hidden files.

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# alias & scripts
source ~/.dotfiles/.common_bash_zsh
[ -f ~/.common-bash-zsh-specific ] && source ~/.common-bash-zsh-specific

# fzf plugin
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# history size configuration
export HISTFILE=$HOME/.zsh_history
export HISTSIZE=100
export SAVEHIST=100

# pc specific
[ -f ~/.zshrc-specific ] && source ~/.zshrc-specific
[ -f ~/.common-bash-zsh-specific ] && source ~/.common-bash-zsh-specific

# Load zsh-syntax-highlighting; should be last.
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

